# Assignment 6 - Komputer Store
- by Ådne Rosenvinge

Heroku: https://komputerstore.herokuapp.com/

The program looks the best on a smaller display, if the screen is to large the ```scrollIntoView``` doesn't work as intended...

## Description

This program is a simple frontend applicaton that simulates: working to earn monney, putting this monney in a bank, loaning monney from a bank, and the fianly spending this monney on a laptop.

All the laptops are fake 'mock' laptops written by me. If you for some reason don't like them, or want to add new once this can easely be done in ``` Laptop.js ``` , just remember to add new images to the ```resources/laptops``` folder.

## Program Files: 

```
client
        index.html -> main screen
        index.js -> logic to manipulate index.html
        /script/
                Laptop.js -> The laptop class, and exported list of the laptops avalible
                BankLoan.js -> Bank Loan class
                PlayerBankBalance.js -> Player bank balance class
                PlayerWorkPay.js -> Player work/pay logic
                DocElements.js -> All document elements reffrences
        style/
                indec.css -> all styles 
        resources/
                favicon_io/
                        Browser icon
                laptops/
                        All laptop images
```

## Design

I went for a clean design, nothing fancy. Found some css templates and and examples (for example the buttons) and were inspired by them. Everything else is written by me.

