

// Laptop data
import {LAPTOPS} from "./script/Laptop.js";

// class with reference to all Document Elements we need
import {DR} from "./script/DocElements.js";

// Work/pay balance logic
import {playerWorkPay} from "./script/PLayerWorkPay.js";

// Bank balance logic
import {playerBankBalance} from "./script/PlayerBankBalance.js";

// Loan logic
import {bankLoan} from "./script/BankLoan.js";


// Creating laptop options
for (const laptop of LAPTOPS){
    const elLaptopOption = document.createElement('option')
    elLaptopOption.value = laptop.id
    elLaptopOption.innerText = laptop.name
    DR.elLaptopSelector.appendChild(elLaptopOption)
}

// Updates all values in the HTML document
function updateDisplay(){
    playerBankBalance.updateDisplayBalance()
    playerWorkPay.updateDisplayBalance()
    bankLoan.updateDisplayLoan()
}


/**
 * Logic connected to applying for loans, and paying loans
 *
 * */
DR.elApplyLoanBtn.addEventListener('click', function (){
    let applyAmount = Number(DR.elLoanAmountInput.value)
    DR.elLoanAmountInput.value = ""
    let message = "" // to be displayed

    if (bankLoan.getCurrentLoan() === 0){ // User have no loans

        // Check if loan is valid
        if (applyAmount <= playerBankBalance.getBalance()*2 && applyAmount > 0){
            message = "Loan approved for " + applyAmount
            playerBankBalance.addToBalance(applyAmount)
            bankLoan.setLoan(applyAmount)
        }else{
            message = "You can only apply for loans greater than 0 and less then half the amount in your account"
        }
    }else{
        // User have loan -> now to pay back
        if (applyAmount <= playerBankBalance.getBalance()){ // If user can pay
            if (applyAmount > bankLoan.getCurrentLoan()) applyAmount = bankLoan.getCurrentLoan()

            bankLoan.setLoan(bankLoan.getCurrentLoan() - applyAmount)
            playerBankBalance.removeFromBalance(applyAmount)

            message = "Loan payed " + applyAmount + ", left to pay: " + bankLoan.getCurrentLoan()

            if (bankLoan.getCurrentLoan() === 0){
                message = "Loan is payed, you can now apply for new loans"
            }
        } else {
            message = "You can't pay more than you have in your bank account"
        }
    }
    console.log(message)
    window.alert(message)
    updateDisplay()
})

/**
 * Event listener for 'loan button'
 * Makes the loan menu visible/hidden
 * */
DR.elLoanBtn.addEventListener("click", function (){
    if (DR.elLoanContainer.style.visibility === 'hidden'){
        DR.elLoanContainer.style.visibility = "visible"
    }else{
        DR.elLoanContainer.style.visibility = "hidden";
    }
    console.log("visibility set to: ", DR.elLoanContainer.style.visibility)
})

/**
 * Event listener for 'work button'
 * Simulates one period of work, adds 100 to the 'work balance'
 * */
DR.elWorkBtn.addEventListener("click", function(){
    playerWorkPay.work()
    console.log("Button clicked -> WORK +" + playerWorkPay.getBalance())
    updateDisplay()
})

/**
 * Event listener for 'bank button'
 * Adds the current work balance to the bank balance
 * */
DR.elBankBtn.addEventListener("click", function (){
    playerBankBalance.addToBalance(playerWorkPay.getBalance())
    console.log("Button clicked -> BANK added to bank: " + playerWorkPay.getBalance())
    playerWorkPay.setBalance(0)
    updateDisplay()
})

/**
 * Event listener for 'BUY button'
 * Just checks the current price of the displayed computer
 * and removes this amount from the bank if the user have enough
 * */
DR.elDisplayLaptopBuyBTN.addEventListener("click", function (){
    const laptopPrice = Number(DR.elDisplayLaptopPrice.innerText)
    if (playerBankBalance.getBalance() < laptopPrice){
        window.alert("You can't afford this item")
    }else{
        playerBankBalance.removeFromBalance(laptopPrice)
        updateDisplay()
        window.alert("You are now the owner of a '" + DR.elDisplayLaptopName.innerText + "'. Thank you for the money")
    }

})


// Helper function for the laptop selector listener
function findSelectedLaptop(id){
    for (let laptop of LAPTOPS) {
        if (id === laptop.id) return laptop
    }
}

/**
 * Event listener for 'Laptop selector'
 * Displays the information about the laptop in the right section
 * */

DR.elLaptopSelector.addEventListener("change", function(){
    const currentLaptop = findSelectedLaptop(Number(this.value))
    console.log(this.value + " " +  currentLaptop.name)

    DR.elDisplayLaptopName.innerText = currentLaptop.name
    DR.elDisplayLaptopPrice.innerText = currentLaptop.price
    DR.elDisplayLaptopDescription.innerText = currentLaptop.description

    // Setting alt
    DR.elDisplayLaptopImage.src = currentLaptop.image
    DR.elDisplayLaptopImage.alt = currentLaptop.description
    DR.elDisplayLaptopImage.width = 200

    DR.elDisplayLaptopFeatures.innerText = ""

    for (let feature of currentLaptop.features){
        const elDisplayFeatures = document.createElement('li')
        elDisplayFeatures.innerText = feature
        DR.elDisplayLaptopFeatures.appendChild(elDisplayFeatures)
    }
    DR.elDisplayLaptopBuyBTN.className = 'btn'
    DR.elDisplayLaptopBuyBTN.innerText = "Buy"

    // Scrolls down to the items tat just popped up
    DR.elDisplayLaptopFeatures.scrollIntoView(true)

})
