

// this class controls the bank loans
import {DR} from "./DocElements.js";

export const bankLoan = {
    currentLoan: 0,
    setLoan: function (amount){
        this.currentLoan = amount
    },
    getCurrentLoan: function (){
        return this.currentLoan
    },
    updateDisplayLoan: function (){
        if (this.currentLoan === 0){
            DR.elCurrentLoan.innerText = "None"
            DR.elApplyLoanBtn.innerText = "Apply"

        }else{
            DR.elCurrentLoan.innerText = this.currentLoan
            DR.elApplyLoanBtn.innerText = "Pay back"
        }
    }
}