

class DocumentElement{
    constructor() {
    // Work section
    this.elWorkBtn = document.getElementById("work-btn")
    this.elBankBtn = document.getElementById("bank-btn")
    this.elPlayerBalance = document.getElementById("player-work-increase")
    // Bank section
    this.elBankBalance = document.getElementById("bank-balance")
    this.elLoanBtn = document.getElementById("loan-btn")
    this.elLoanContainer = document.getElementById("apply")
    // Loan section
    this.elApplyLoanBtn = document.getElementById("apply-loan-btn")
    this.elLoanAmountInput = document.getElementById('loan-amount')
    this.elCurrentLoan = document.getElementById('current-loan')
    // Laptop selection
    this.elLaptopSelector = document.getElementById("laptop-selector")
    // Laptop display section
    this.elDisplayLaptopName = document.getElementById("laptop-name")
    this.elDisplayLaptopPrice = document.getElementById("laptop-price")
    this.elDisplayLaptopDescription = document.getElementById("laptop-description")
    this.elDisplayLaptopImage = document.getElementById("laptop-image")
    this.elDisplayLaptopFeatures = document.getElementById("laptop-features")
    this.elDisplayLaptopBuyBTN = document.getElementById("buy-laptop-btn")
    }
}

export const DR = new DocumentElement() // DR -> Document References