
/**
 * Laptop class with constructor
 * Also an exported variable with all
 * */

class Laptop {
    constructor(id, name, price, description, image, features) {
        this.id = id
        this.name = name
        this.price = price
        this.description = description
        this.image = image
        this.features = features
    }
}

export const LAPTOPS = [
    new Laptop(
        1,
        "Von Laptop XIX Plus Elite PRO S",
        5000,
        "This is our best seller, weary nice looking computer with both keyboard and screen included (the battery is sold separately (39'999 NOK + shipping))",
        "resources/laptops/laptop1.png",
        ["Keyboard", "Screen", "Von Laptop logo", "The newest U2 album (It can not be removed)"]
    ),
    new Laptop(
        2,
        "Smart book A.V.S.D.T.F.S.L.K.V.10",
        200,
        "This is a terrible choice. Both hardware and software is totally outdated, and will probably break within the first week.",
        "resources/laptops/laptop2.png",
        ["Keyboard (The ctrl-button is missing)", "Cracked Screen", "Windows 64 (Image is misleading)"]
    ),
    new Laptop(
        3,
        "Ultra sonic Gamer 2000",
        6000,
        "This company didn't pay us to promote their products. They didn't even give us a proper photo of the laptop. I mean, look at this?? the background isn't even transparent",
        "resources/laptops/laptop3.png",
        ["Probably a dedicated graphics card idk?"]
    ),
    new Laptop(
        4,
        "10'000 kr Netflix Machine",
        10000,
        "Forget about the computer. Look at this guy. Can you imagine? I can't even se the guy, only those muscles. This guy have to walk sideways through doors, or does he have special made doors at home?",
        "resources/laptops/laptop4.jpg",
        ["Keyboard with the classic 'Netflix' button", "screen", "WiFi", "The beefy dude is included"]
    )
]