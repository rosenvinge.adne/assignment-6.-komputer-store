import {DR} from "./DocElements.js";

// This class controls the 'work balance'
export const playerWorkPay = {
    balance: 0,
    work: function() {
        this.balance += 100
    },
    setBalance: function(newB){
        this.balance = newB
    },
    getBalance: function(){
        return this.balance
    },
    updateDisplayBalance: function (){
        console.log("UPDATE")
        DR.elPlayerBalance.innerText = this.balance
    }
}