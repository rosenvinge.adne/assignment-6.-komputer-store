import {DR} from "./DocElements.js";
import {bankLoan} from "./BankLoan.js";

// PLayer Bank Balance, putting money in the bank and spending
export const playerBankBalance = {
    balance: 0,
    getBalance: function(){
        return this.balance
    },
    addToBalance: function(amount){

        if (bankLoan.getCurrentLoan() !== 0){
            bankLoan.setLoan(bankLoan.getCurrentLoan()-amount/10)
            this.balance += amount - amount/10
        }else{
            this.balance += amount
        }
    },
    removeFromBalance: function (amount){
        this.balance -= amount
    },
    updateDisplayBalance: function (){
        DR.elBankBalance.innerText = this.balance
    }
}