
const express = require('express')
const app = express()
const path = require('path')
const {PORT = 3000} = process.env

app.use(express.static(path.join(__dirname, "client")))

app.get('/', (req, res) => {
    res.sendFile('index.html', path.join(__dirname, '/client'));
})

app.listen(PORT, () => {
    console.log('Server start on port: ' + PORT)
})
